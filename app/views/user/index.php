<style>
    .mx-auto {
        width: 800px;
    }

    .card {
        margin-top: 10px;
    }
</style>
<div class="mx-auto">
    <div class="card">
        <div class="card-header text-white bg-primary">
            Create/Edit Data
        </div>
        <div class="card-body">
            <form action="" method="POST">
                <div class="mb-3 row">
                    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="nim" name="nim">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="nama" name="nama">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="prodi" class="col-sm-2 col-form-label">Jurusan</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="prodi" name="prodi">
                            <option value="">- Pilih Jurusan -</option>
                            <option value="Multimedia">Multimedia</option>
                            <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                            <option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
                        </select>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
                    </div>
                </div>
                <div
            </form>
        </div>
    </div>
</div>