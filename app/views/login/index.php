<link rel="stylesheet" href="<?= BASE_URL ?>/css/bootstrap.css">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card my-5 shadow-lg" style="width: 30rem;">
                <div class="card-body">
                    <h2 class="card-title text-center">Login</h2>
                    <br></br>
                    <form action="<?= BASE_URL ?>/login/sign" method="post">
                        <div class="form-group mb-3">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username">
                        </div>
                        <div class="form-group mb-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="form-group mb-3">
                            <input type="submit" name="submit" value="Login" class="btn btn-primary">
                        </div>
                        <a href="<?= BASE_URL ?>/register">Don't have an account?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>